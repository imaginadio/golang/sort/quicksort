package quicksort

func QuickSort(arr []int) []int {
	l := len(arr)
	if l <= 1 {
		return arr
	}

	el := arr[l/2]
	var left, right []int
	for i, v := range arr {
		if i == l/2 {
			continue
		}

		if v < el {
			left = append(left, v)
		} else {
			right = append(right, v)
		}

	}

	return append(append(QuickSort(left), el), QuickSort(right)...)
}
