package quicksort

import (
	"testing"

	"github.com/go-playground/assert/v2"
)

func TestQuickSort(t *testing.T) {
	unsorted := []int{5, 3, 7, 7, 4, 9, 1}
	expected := []int{1, 3, 4, 5, 7, 7, 9}

	t.Run("OK", func(t *testing.T) {
		sorted := QuickSort(unsorted)
		assert.Equal(t, expected, sorted)
	})
}
